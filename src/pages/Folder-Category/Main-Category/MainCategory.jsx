import React, { useEffect, useState } from "react";
import { Switch } from "@mui/material";
import { useTranslation } from "react-i18next";
import Actions from "../../../components/Actions/Actions";
import { useDispatch } from "react-redux";
import HeaderPage from "../../../components/HeaderPage/HeaderPage";
import Navbar from "../../../components/Navbar/Navbar";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import Datatable from "../../../components/datatable/Datatable";
import { getAllCategory } from "../../../store/categorySlice";
import { useSelector } from "react-redux";

function MainCategory() {
  const [t, i18n] = useTranslation();
  const label = { inputProps: { "aria-label": "Size switch demo" } };

  let data = [
    {
      id: 1,
      name: "Runk al dena al khairy",
    },
    {
      id: 2,
      name: "muhdey al khairy",
    },
    {
      id: 3,
      name: "Mazhe al khairy",
    },
  ];

  const userColumns = [
    { field: "id", headerName: t("ID"), width: 80 },
    {
      field: "name",
      headerName: t("Name"),
      width: 750,
    },
    {
      field: "Action",
      headerName: t("Action"),
      sortable: false,
      filter: false,
      width: 200,
      renderCell: (params) => {
        return (
          <div>
            <Actions filter="Categories" params={params} />
          </div>
        );
      },
    },
  ];

  const [selectedRowIds, setSelectedRowIds] = useState([]);
  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
    console.log("Selected Row IDs:", selectionModel);
  };

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllCategory({ lang: i18n.language }));
  }, []);

  let {
    data: categories,
    loading,
    errorAll,
  } = useSelector((state) => state.categories);

  return (
    <div className="col-xl-10 col-lg-12 mt-1">
      <Navbar />
      <div className="two-box">
        <HeaderPage
          data={categories}
          selectedRowIds={selectedRowIds}
          title="Categories"
          filter="Categories"
        />
        <div className="table">
          {errorAll ? (
            <ErrorCompo />
          ) : loading ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <Datatable
              userColumns={userColumns}
              userRows={categories && categories}
              onSelectionModelChange={handleSelectionChange}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default MainCategory;
