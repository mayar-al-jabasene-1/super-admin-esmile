import React, { useEffect, useState } from "react";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import Datatable from "../../../components/datatable/Datatable";
import { useDispatch } from "react-redux";
import Navbar from "../../../components/Navbar/Navbar";
import HeaderPage from "../../../components/HeaderPage/HeaderPage";
import Actions from "../../../components/Actions/Actions";
import { useTranslation } from "react-i18next";
import { getAllChronicD } from "../../../store/chronicDSlice";
import { useSelector } from "react-redux";

function MainChronicD() {
  const [t, i18n] = useTranslation();
  const label = { inputProps: { "aria-label": "Size switch demo" } };

  let data = [
    {
      id: 1,
      name: "Diabetes mellitus",
    },
    {
      id: 2,
      name: "Hypertension (high blood pressure)",
    },
    {
      id: 3,
      name: "Asthma",
    },
    {
      id: 4,
      name: "Chronic obstructive pulmonary disease (COPD)",
    },
    {
      id: 3,
      name: "Rheumatoid arthritis",
    },
  ];

  const userColumns = [
    { field: "id", headerName: t("ID"), width: 80 },
    {
      field: "name",
      headerName: t("Name"),
      width: 750,
    },
    {
      field: "Action",
      headerName: t("Action"),
      sortable: false,
      filter: false,
      width: 200,
      renderCell: (params) => {
        return (
          <div>
            <Actions filter="ChronicD" params={params} />
          </div>
        );
      },
    },
  ];

  const [selectedRowIds, setSelectedRowIds] = useState([]);
  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
    console.log("Selected Row IDs:", selectionModel);
  };

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllChronicD({ lang: i18n.language }));
  }, []);

  let {
    data: chronicd,
    loading,
    errorAll,
  } = useSelector((state) => state.chronicd);
  return (
    <div className="col-xl-10 col-lg-12 mt-1">
      <Navbar />
      <div className="two-box">
        <HeaderPage
          data={chronicd}
          selectedRowIds={selectedRowIds}
          title="Chronic Diseases"
          filter="ChronicD"
        />
        <div className="table">
          {errorAll ? (
            <ErrorCompo />
          ) : loading ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <Datatable
              userColumns={userColumns}
              userRows={chronicd && chronicd}
              onSelectionModelChange={handleSelectionChange}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default MainChronicD;
