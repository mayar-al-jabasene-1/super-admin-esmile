import { DialogTitle, TextField, Typography } from "@mui/material";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import React, { Fragment, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import CheckMultiLangu from "../../../components/CheckMultiLangu/CheckMultiLangu";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import {
  createAccessMethod,
  getAllAccessMethod,
  getSingleAccessMethod,
  updateAccessMethod,
} from "../../../store/accessMethod";
import { useSelector } from "react-redux";

function PopUpAccessM({ type, handleClose, data }) {
  let [inp, setInp] = useState("");

  const [t, i18n] = useTranslation();

  let dispatch = useDispatch();

  let {
    data: accessmethod,
    loadingBTN,
    error,
    dataCreate,
    dataUpdate,
    loadingSingle,
    errorSingle,
    singleData,
  } = useSelector((state) => state.accessmethod);

  useEffect(() => {
    if (dataCreate || dataUpdate) {
      dispatch(getAllAccessMethod({ lang: i18n.language }));
      handleClose();
    }
  }, [dataCreate, dataUpdate]);

  useEffect(() => {
    if (type === "edit") {
      dispatch(getSingleAccessMethod({ id: data.id, lang: i18n.language }));
    }
  }, [type]);

  let getValue = (e) => {
    setInp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let onsubmitfn = (e) => {
    e.preventDefault();
    if (type === "add") {
      console.log("addd");
      dispatch(createAccessMethod({ data: inp, lang: i18n.language }));
    }
    if (type === "edit") {
      console.log("ediit");
      let newdata = {
        ...inp,
        _method: "PUT",
      };
      dispatch(
        updateAccessMethod({
          id: singleData.id,
          data: newdata,
          lang: i18n.language,
        })
      );
    }
  };

  // Step 1: Create state to store selected languages
  const [selectedLanguages, setSelectedLanguages] = useState(["en"]);

  const toggleLanguage = (lang) => {
    if (selectedLanguages.includes(lang)) {
      // If the language is already selected, remove it
      setSelectedLanguages(selectedLanguages.filter((l) => l !== lang));
    } else {
      // If the language is not selected, add it
      setSelectedLanguages([...selectedLanguages, lang]);
    }
  };

  console.log("inpp<...>", inp);
  return (
    <div>
      <DialogTitle sx={{ p: 3 }}>
        <Typography
          variant="h5"
          gutterBottom
          style={{
            borderBottom: "1px solid lightgray",
            paddingBottom: "2px",
            marginBottom: "30px",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          {type === "edit" ? "Edit Access Method" : "Add Access Method"}

          <CheckMultiLangu
            toggleLanguage={toggleLanguage}
            selectedLanguages={selectedLanguages}
          />
        </Typography>

        {errorSingle ? (
          <ErrorCompo />
        ) : loadingSingle ? (
          <div className="loading">
            <CircularIndeterminate />
          </div>
        ) : (
          <form class="row" onSubmit={(e) => onsubmitfn(e)}>
            {selectedLanguages.map(
              (lang) =>
                selectedLanguages.includes(lang) && (
                  <div className="col-md-12 mb-3">
                    <div className="form-group">
                      <TextField
                        id={"text"}
                        type={"text"}
                        label={t("name") + ` (${lang})`}
                        name={`name[${lang}]`}
                        required
                        onChange={(e) => getValue(e)}
                        variant="standard"
                        style={{ width: "100%" }}
                      />
                    </div>
                  </div>
                )
            )}

            <div className="btn-lest mt-3">
              {loadingBTN ? (
                <button type="text" disabled className="btn btn-primary-rgba ">
                  <CheckCircleIcon color="#fff" /> {t("Loading")}...
                </button>
              ) : (
                <button type="submit" className="btn btn-primary-rgba">
                  <CheckCircleIcon color="#fff" />
                  {type === "edit" ? t("Edit") : t("Craete")}
                </button>
              )}

              <span onClick={handleClose} className="btn btn-danger-rgba">
                <DoNotDisturbAltIcon color="#fff" /> {t("Cancel")}
              </span>
            </div>
          </form>
        )}
      </DialogTitle>
    </div>
  );
}

export default PopUpAccessM;
