import React, { useEffect, useState } from "react";
import "./MainUsers.scss";
import { useTranslation } from "react-i18next";
import { Switch } from "@mui/material";
import Actions from "../../../components/Actions/Actions";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "../../../components/Navbar/Navbar";
import Datatable from "../../../components/datatable/Datatable";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import HeaderPage from "../../../components/HeaderPage/HeaderPage";
import { getAlladmins, resetsingleDataAdmins } from "../../../store/adminslice";

function MainUsers() {
  const [t, i18n] = useTranslation();
  const label = { inputProps: { "aria-label": "Size switch demo" } };

  const userColumns = [
    { field: "id", headerName: t("ID"), width: 100 },
    {
      field: "name",
      headerName: t("Name"),
      width: 310,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.img ? (
              <img className="cellImg" src={params.row.img} alt="avatar" />
            ) : (
              <div className="boxImageChar">
                <span>{params.row.name.slice(0, 1)}</span>
              </div>
            )}
            {`${params.row.name}`}
          </div>
        );
      },
    },
    {
      field: "email",
      headerName: t("Email"),
      width: 250,
    },
    {
      field: "role",
      headerName: t("Role"),
      width: 200,
      renderCell: (params) => {
        return (
          <div className="d-flex justify-content-between align-items-start gap-2 flex-column">
            <div>{params.row.role}</div>
          </div>
        );
      },
    },
    {
      field: "status",
      headerName: t("Status"),
      width: 120,
      renderCell: (params) => {
        return (
          <div className="status">
            <Switch
              {...label}
              defaultChecked={params.row.active === 1 ? true : false}
            />
          </div>
        );
      },
    },

    {
      field: "Action",
      headerName: t("Action"),
      sortable: false,
      filter: false,
      width: 150,
      renderCell: (params) => {
        return (
          <div>
            <Actions filter="users" params={params} />
          </div>
        );
      },
    },
  ];

  // let getStatus = (e, id) => {
  //   let data = {
  //     model_id: id,
  //     model_class: "Driver",
  //     attribute: "status",
  //     value: e.target.checked ? 1 : 0,
  //   };
  //   dispatch(ChangeStatus(data));
  // };

  const [selectedRowIds, setSelectedRowIds] = useState([]);
  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
    console.log("Selected Row IDs:", selectionModel);
  };

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAlladmins({ lang: i18n.language }));
    dispatch(resetsingleDataAdmins(""));
  }, []);

  let {
    data: admins,
    loading,
    errorAll,
  } = useSelector((state) => state.admins);

  return (
    <div className="col-xl-10 col-lg-12 mt-1">
      <Navbar />
      <div className="two-box">
        <HeaderPage
          data={admins}
          selectedRowIds={selectedRowIds}
          title="Admins"
          filter="users"
        />
        <div className="table">
          {errorAll ? (
            <ErrorCompo />
          ) : loading ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <Datatable
              userColumns={userColumns}
              userRows={admins && admins}
              onSelectionModelChange={handleSelectionChange}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default MainUsers;
