// routes.js
import { Navigate } from "react-router-dom";
import Login from "./pages/login/Login";
import AdminHome from "./pages/AdminHome/AdminHome";
import NotFound from "./pages/NotFoundPage/NotFound";
import MainUsers from "./pages/Folder-Users/MainUsers/MainUsers";
import MainCompany from "./pages/Folder-Comapny/Main-Company/MainCompany";
import MainCategory from "./pages/Folder-Category/Main-Category/MainCategory";
import MainChronicD from "./pages/FolderChronicD/MainChronicD/MainChronicD";
import MainAccessM from "./pages/Folder-AccessMethod/MainAccessM/MainAccessM";
import LanguSpeak from "./pages/Folder-LanguageSpeak/MainLanguSpeak/LanguSpeak";
import MainSubscription from "./pages/Folder-Subscription/MainSubscription/MainSubscription";
import RoleAndPermision from "./pages/Folder-role/RoleAndPermision/RoleAndPermision";
import RolePermEditAdd from "./pages/Folder-role/RolePermisionEditAdd/RolePermEditAdd";

const useRout = (token, sidebarOpen) => {
  let test = true;
  const routes = [
    { path: "/", element: token ? <AdminHome /> : <Login /> },
    {
      path: "/login",
      element: token ? <Navigate to="/" replace /> : <Login />,
    },
    { path: "/admins", element: <MainUsers /> },
    { path: "roles-permision", element: <RoleAndPermision /> },
    { path: "roles-permision/:name/:id", element: <RolePermEditAdd /> },
    { path: "roles-permision/:name", element: <RolePermEditAdd /> },
    { path: "/companies", element: <MainCompany /> },
    { path: "/subscription", element: <MainSubscription /> },
    { path: "/categories", element: <MainCategory /> },
    { path: "/chronic-diseases", element: <MainChronicD /> },
    { path: "/access-methods", element: <MainAccessM /> },
    { path: "/language-speak", element: <LanguSpeak /> },
    { path: "*", element: <NotFound /> },
  ];
  return routes;
};

export default useRout;
