import { useDispatch } from "react-redux";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
// import { getAllPermissionsProject } from "./store/roleSlice";

// function UsePermissions(setopenInternet) {
//   let { dataPermP, loadingPermP, errorPermP } = useSelector(
//     (state) => state.roles
//   );

//   let dispatch = useDispatch();
//   useEffect(() => {
//     dispatch(getAllPermissionsProject());
//   }, []);
//   return dataPermP;
// }

function checkpermation(requiredPermissions, userPermission) {
  let NamesUserPermission = userPermission && userPermission.map((e) => e.name);
  let NameRequiredPermissions =
    requiredPermissions && requiredPermissions.map((e) => e.name);
  let MatchingResult =
    NamesUserPermission &&
    NamesUserPermission.filter((names) =>
      NameRequiredPermissions.includes(names)
    );
  return (
    NameRequiredPermissions &&
    NameRequiredPermissions.every(
      (e) => MatchingResult && MatchingResult.includes(e)
    )
  );
}

export default checkpermation;
