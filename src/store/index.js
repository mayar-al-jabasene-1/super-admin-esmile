import { combineReducers, configureStore } from "@reduxjs/toolkit";
import admins from "./adminslice";
import auth from "./authslice";
import roles from "./roleSlice";
import companies from "./companySlice";
import categories from "./categorySlice";
import chronicd from "./chronicDSlice";
import accessmethod from "./accessMethod";
import subscription from "./subscriptionSlice";
import languspeak from "./LanguageSpeakSlice";
import commonlists from "./commonSlice";
import themeslice from "./theme";

import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
};

// Create a persistConfig for the auth slice
const authPersistConfig = {
  key: "auth",
  storage: storage,
  whitelist: ["data"], // Specify the properties you want to persist
};

const customReducer = combineReducers({
  themeslice: themeslice,
  auth: persistReducer(authPersistConfig, auth),
  // Add other reducers as needed
});

const persistCustomReducer = persistReducer(persistConfig, customReducer);

let store = configureStore({
  reducer: {
    persistTheme: persistCustomReducer,
    admins: admins,
    roles: roles,
    commonlists: commonlists,
    companies: companies,
    categories: categories,
    subscription: subscription,
    accessmethod: accessmethod,
    chronicd: chronicd,
    languspeak: languspeak,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export let persistor = persistStore(store);

export default store;
