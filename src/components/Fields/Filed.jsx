import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import FormControl from "@mui/material/FormControl";
import Input from "@mui/material/Input";
import { useTranslation } from "react-i18next";

function Field({ type, name, defaultvalue, required, label, icon, getValue }) {
  const [showPassword, setShowPassword] = React.useState(false);
  const [t, i18n] = useTranslation();
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  return (
    <Box
      component="form"
      sx={{
        "& .MuiTextField-root": {
          m: 1,
          width: "100%",
        },
        ".css-1c2i806-MuiFormLabel-root-M": {},
        ".css-wgai2y-MuiFormLabel-asterisk": {
          color: "red",
        },
        ".css-1c2i806-MuiFormLabel-root-MuiInputLabel-root.Mui-focused": {},
        ".css-1eed5fa-MuiInputBase-root-MuiInput-root::after": {},
        display: "flex",
        flexWrap: "wrap",
        marginBottom: "15px",
      }}
      noValidate
      autoComplete="off"
    >
      <div style={{ width: "100%" }}>
        {type === "password" ? (
          <FormControl sx={{ m: 1, width: "100%" }} variant="standard">
            <InputLabel htmlFor={name}>
              {t(label)} {required && <sup className="redstar">*</sup>}
            </InputLabel>
            <Input
              id={name}
              type={showPassword ? "text" : "password"}
              name={name}
              required={required}
              onChange={(e) => getValue(e)}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
        ) : type === "number" ? (
          <TextField
            required={required}
            id={name}
            type={type}
            label={t(label)}
            name={name}
            defaultValue={defaultvalue}
            onChange={(e) => getValue(e)}
            InputLabelProps={{
              shrink: true,
            }}
            variant="standard"
          />
        ) : (
          <TextField
            required={required}
            id={name}
            type={type}
            label={t(label)}
            name={name}
            onChange={(e) => getValue(e)}
            defaultValue={defaultvalue}
            variant="standard"
          />
        )}
      </div>
    </Box>
  );
}

export default Field;
