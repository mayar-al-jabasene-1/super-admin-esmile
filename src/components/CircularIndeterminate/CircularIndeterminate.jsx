import * as React from "react";
import "./CircularIndeterminate.scss";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

export default function CircularIndeterminate() {
  return <span className="loader"></span>;
}
